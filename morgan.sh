#!/usr/bin/env bash
#
# Tool for organizing moviez.
#
# @file: movie organizer
# @author: q©memoize.se
# @version: 20210313
# @license: MIT License (https://opensource.org/licenses/MIT)

### dev notes
# backdrop url: https://image.tmdb.org/t/p/original/r7xf2vnAIgMDTndfnltfzqjNwqk.jpg
# cover url: https://image.tmdb.org/t/p/w780/kBMHVEbMoV9TTzijZSUhAWYd2am.jpg
# no api key needed
#

### configuration

# api key for developers.themoviedb.org
api_key=

# max number of actors to store per movie
max_actors=10

# directory defaults (preferrably in the root of your collection)
meta_dir=./.meta

### end configuration

function usage {
	echo "\
	Usage: morgan [options] [pattern]

	Movie organizer creates metadata directories with symlinks
	to the original directories for all or some directories
	in the current directory tree.

	It can also clean up said tree (prune) or create an
	alphabetical index for the directories in the current
	directory (organize).

	Options:
	    -d        dry run, download metadata but create no links
	    -f        force download of metadata even if cached
	    -h        this help
	    -k        api key (if you do not want to edit the script)
	    -m        max actors per movie to index (default is 5)
	    -o        move directories into first-letter super-dirs
	    -p        delete broken symlinks (prune tree) then exit
	    -v        be verbose

	Requirements:
	    - an api key to the movie database api
	    - bc, jq and curl
	    - directory names must at least contain a movie title and a
	      four-digit-year in that order

	Links:
	https://developers.themoviedb.org/4/getting-started/authorization
	https://stedolan.github.io/jq/download/
	" | sed -E 's/	//g;'

	[[ $1 ]] && echo "error: $1"

	exit 0
}

function prune {
	[[ $verbose ]] && print="-print" || print=""
	find -L "$meta_dir" -type l $print -exec rm -- {} +
	exit 0
}

function organize {
	find . -iname "$search" -not -path '*/.*' -type d -d 1 -print0 | while read -d $'\0' dir
	do
		base=$(basename "$dir")
		if (( ${#base} > 7 )); then
			to=$(echo ${base:0:5} | tr '[:upper:] ' '[:lower:].')
			to=${to/#the./}
			to=${to:0:1}

			if [[ -z $dry_run && ! -e "$to/$base" ]]; then
				[[ -d "$to" ]] || mkdir "$to"
				mv "$dir" "$to/$base"
			fi

			[[ $verbose ]] && echo "move $to/$base"
		fi
	done
}

# internal vars etc
set -e -o pipefail
action=
search=*
dry_run=
force=
verbose=
tmdb_api="https://api.themoviedb.org/3"
ifs=$IFS
wd=$(pwd -P)
cast_dir=$meta_dir/actor
year_dir=$meta_dir/decade
vote_dir=$meta_dir/rating
genre_dir=$meta_dir/genre
fail_dir=$meta_dir/fail
cache_dir=$meta_dir/.cache
genre_cache=$cache_dir/genres.json

# sanity check
for cmd in bc jq curl; do hash $cmd 2> /dev/null ||\
usage "missing $cmd, see requirements"; done

[[    $api_key   ]] || usage api-key
[[ -d $meta_dir  ]] || mkdir -v $meta_dir
[[ -d $cache_dir ]] || mkdir -v $cache_dir
[[ -d $cast_dir  ]] || mkdir -v $cast_dir
[[ -d $year_dir  ]] || mkdir -v $year_dir
[[ -d $vote_dir  ]] || mkdir -v $vote_dir
[[ -d $fail_dir  ]] || mkdir -v $fail_dir

# this is supposed to make plex ignore folder
echo "*" > $meta_dir/.plexignore

while getopts ":dfhk:m:opvs:" options; do
	case "${options}" in
		d) dry_run=1 ;;
		f) force=1 ;;
		k) api_key=${OPTARG} ;;
		m) max_actors=${OPTARG} ;;
		o) action=organize ;;
		p) action=prune ;;
		v) verbose=1 ;;
		*) usage ;;
	esac
done
shift $((OPTIND - 1))
if [[ "$#" -ne 0 ]]; then
	search="*$@*"
fi

if [[ $action ]]; then $action; exit 0; fi

if [[ $force || ! -f $genre_cache ]]; then
	curl -fo "$genre_cache" "$tmdb_api/genre/movie/list?api_key=${api_key}&language=en-US" 2> /dev/null
fi

for genre in $(jq '.genres[] |  [ .id, .name ] | @csv' < "$genre_cache" | sed -E 's/[\"]//g; s/ /_/g;'); do
	IFS=","
	read gid name <<< "$genre"
	genres[$gid]=$(tr '[:upper:]' '[:lower:]' <<< $name)
	IFS=$ifs
done

find "$(pwd)" -iname "$search" -type d -not -path '*/.*' -print0 | while read -d $'\0' dir
do
	echo before="$dir"

	base=$(basename "$dir")
	echo base="$base"

	unscene=($(
		sed -E '
			s/[^A-Za-z0-9 .]//g;
			s/[. ]+/./g;
			s/[0-9]+p//g;
			s/UNCENSORED|UNRATED|REMASTERED|DIRECTORS.CUT|PROPER//g;
			s/(.*)([0-9]{4})[^p]?.*/\1 \2/;
		' <<< "$base"))

	name=${unscene[0]//./ }
	year=${unscene[1]}

	if [[ -z $year ]]; then
		>&2 echo "parser failed for: $base"
		continue
	fi

	movie_cache=${cache_dir}/${name// /_}.json
	cast_cache=${cache_dir}/${name// /_}_cast.json

	if [[ ! $force &&  -f $movie_cache ]]; then
		[[ $verbose ]] && echo "skipping $base ($name, $year)"
		continue
	else
		echo "processing $base ($name, $year)"
	fi

	if [[ $force || ! -f $movie_cache ]]; then
		query=${name// /+}
		curl -fo "$movie_cache" \
		"$tmdb_api/search/movie?api_key=${api_key}&language=en-US&query=${query}&page=1&include_adult=true&year=${year}" 2> /dev/null
	fi

	read -r results movie_id vote_avg genreids <<< $(\
		jq '.total_results, .results[0].id, .results[0].vote_average, (.results[0].genre_ids | join(" "))' \
		< "$movie_cache")

	if [[ $results == 0 ]]; then
		>&2 echo "no metadata for $name"
		ln -sf "$dir" "$fail_dir"
		continue
	fi

	if [[ $vote_avg != "null" ]]; then
		vote=$(bc <<< "$(printf %1.f "$(bc <<< "$vote_avg * 2")") * 0.5")
		by_vote_dir=$vote_dir/$vote

		if [[ -z $dry_run && ! -e "$by_vote_dir/$base" ]]; then
			mkdir -p "$by_vote_dir"
			ln -sf "$dir" "$by_vote_dir"
		fi

		[[ $verbose ]] && echo "add $by_vote_dir"
	fi

	genreids=${genreids//\"/}
	for gid in $genreids; do
		by_genre_dir=$genre_dir/${genres[$gid]}
		if [[ -z $dry_run && ! -e "$by_genre_dir/$base" ]]; then
			mkdir -p "$by_genre_dir"
			ln -sf "$dir" "$by_genre_dir"
		fi

		[[ $verbose ]] && echo "add $by_genre_dir"
	done

	by_year_dir=$year_dir/${year:0:3}0
	if [[ -z $dry_run && ! -e "$by_year_dir/$base" ]]; then
		mkdir -p "$by_year_dir"
		ln -sf "$dir" "$by_year_dir"
	fi

	[[ $verbose ]] && echo "add $by_year_dir"

	if [[ $force || ! -f $cast_cache ]]; then
		curl -fo "$cast_cache" \
		"$tmdb_api/movie/${movie_id}/credits?api_key=${api_key}&language=en-US" 2> /dev/null
	fi

	cast=$(jq '.cast[] | select(.known_for_department=="Acting") | .name' < "$cast_cache" | head -${max_actors})

	IFS=$'\n'
	for actor in $cast; do
		# make actor name as safe as possible
		actor=$(echo $actor |\
			iconv -t US-ASCII//TRANSLIT |\
			tr -dc '[:alnum:] ' |\
			tr '[:upper:] ' '[:lower:]_' | xargs)

		actor_dir=$cast_dir/${actor:0:1}/${actor//\"/}

		if [[ -z $dry_run && ! -e "$actor_dir/$base" ]]; then
			[[ -d "$actor_dir" ]] || mkdir -p "$actor_dir"
			ln -sf "$dir" "$actor_dir"
		fi

		[[ $verbose ]] && echo "add $actor_dir"
	done
	IFS=$ifs
done
